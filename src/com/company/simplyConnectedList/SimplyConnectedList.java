package com.company.simplyConnectedList;

public class SimplyConnectedList {

    private SimplyConnectedListNode head;
    private SimplyConnectedListNode tail;

    //works
    public void insertFront(int data) {
        long start = System.nanoTime();
        SimplyConnectedListNode a = new SimplyConnectedListNode();
        a.data = data;
        if(head == null){
            head = a;
            tail = a;
        } else {
            a.next = head;
            head = a;
        }
        long finish = System.nanoTime();
        System.out.println("elapsed time " + getDelta(start, finish));
    }

    //works
    public void insertBack(int data) {
        long start = System.nanoTime();
        SimplyConnectedListNode a = new SimplyConnectedListNode();
        a.data = data;
        if (tail == null) {
            head = a;
            tail = a;
        } else {
            tail.next = a;
            tail = a;
        }
        long finish = System.nanoTime();
        System.out.println("elapsed time " + getDelta(start, finish));
    }

    //works
    public void insertNode(int data, int position){
        SimplyConnectedListNode a = new SimplyConnectedListNode();
        a.data = data;
        SimplyConnectedListNode temp = head;
        int i = 0;
        while (i < position - 2){
            temp = temp.next;
            i++;
        }
        a.next = temp.next;
        temp.next = a;
    }

    //works
    public void changeFront(int data){
        long start = System.nanoTime();
        if(head != null){
            head.data = data;
        }
        long finish = System.nanoTime();
        System.out.println("elapsed time " + getDelta(start, finish));
    }

    //works
    public void changeBack(int data){
        long start = System.nanoTime();
        if(tail != null){
            tail.data = data;
        }
        long finish = System.nanoTime();
        System.out.println("elapsed time " + getDelta(start, finish));
    }

    //works
    public void changeNode(int data, int position){
        SimplyConnectedListNode temp = head;
        int tempPosition = 0;
        while (tempPosition != position){
            temp = temp.next;
            tempPosition++;
        }
        temp.data = data;
    }

    //works
    public void removeBack(){
        if (head == tail) {
            head = null;
            tail = null;
        } else {
            SimplyConnectedListNode current = head;
            while (current.next != tail) {
                current = current.next;
            }
            current.next = null;
            tail = current;
        }
    }

    //works
    public void removeFront(){
        if (head != null){
            head = head.next;
        }
    }

    //works
    public void removeNode(int data) {
        long start = System.nanoTime();
        if(head == null)
            return;

        if (head == tail) {
            head = null;
            tail = null;
            return;
        }

        if (head.data == data) {
            head = head.next;
            return;
        }

        SimplyConnectedListNode t = head;       //иначе начинаем искать
        while (t.next != null) {    //пока следующий элемент существует
            if (t.next.data == data) {  //проверяем следующий элемент
                if(tail == t.next) {     //если он последний
                    tail = t;           //то переключаем указатель на последний элемент на текущий
                }
                t.next = t.next.next;   //найденный элемент выкидываем
                return;                 //и выходим
            }
            t = t.next;                //иначе ищем дальше
        }
        long finish = System.nanoTime();
        System.out.println("elapsed time " + getDelta(start, finish));
    }

    //works
    public int sumNodes(){
        long start = System.nanoTime();
        SimplyConnectedListNode t = head;
        int sum = 0;
        while (t != null){
            sum += t.data;
            t = t.next;
        }
        long finish = System.nanoTime();
        System.out.println("elapsed time " + getDelta(start, finish));
        return sum;
    }

    //works
    public int getNodeNumber(int data){
        long start = System.nanoTime();
        if (head.data == data){
            return 0;
        }

        int number = 0;
        SimplyConnectedListNode t = head;
        while (t.next != null){
            number++;
            if (t.next.data == data){
                return number;
            }
            t = t.next;
        }
        long finish = System.nanoTime();
        System.out.println("elapsed time " + getDelta(start, finish));
        return  -1;
    }

    public void printList() {
        SimplyConnectedListNode t = head;
        while (t != null) {
            System.out.print(t.data + " ");
            t = t.next;
        }
    }

    private double getDelta(long start, long finish){
        return (double) (finish - start)/1000000000.0;
    }
}
