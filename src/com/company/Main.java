package com.company;

import com.company.doublyLinkedList.DoublyLinkedList;

public class Main {

    public static void main(String[] args) {

        DoublyLinkedList dll = new DoublyLinkedList();
        dll.insertBack(1);
        dll.insertBack(2);
        dll.insertBack(3);
        dll.insertBack(4);
        dll.insertBack(5);

        dll.removeNode(3);

        dll.printList();
        System.out.println();

    }
}
