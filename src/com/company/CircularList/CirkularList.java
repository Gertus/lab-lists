package com.company.CircularList;


public class CirkularList {

    private CirkularListNode head;
    private CirkularListNode tail;

    //works
    public void insertFront(int data) {
        long start = System.nanoTime();
        CirkularListNode a = new CirkularListNode();
        a.data = data;
        if(head == null){
            head = a;
            tail = a;
            tail.next = head;
        } else {
            a.next = head;
            head = a;
            tail.next = head;
        }
        long finish = System.nanoTime();
        System.out.println("elapsed time " + getDelta(start, finish));
    }

    //works
    public void insertBack(int data) {
        long start = System.nanoTime();
        CirkularListNode a = new CirkularListNode();
        a.data = data;
        if (tail == null) {
            head = a;
            tail = a;
        } else {
            tail.next = a;
            tail = a;
            tail.next = head;
        }
        long finish = System.nanoTime();
        System.out.println("elapsed time " + getDelta(start, finish));
    }

    //works
    public void insertNode(int data, int position){
        CirkularListNode a = new CirkularListNode();
        a.data = data;
        CirkularListNode temp = head;
        int i = 0;
        while (i < position - 2){
            temp = temp.next;
            i++;
        }
        a.next = temp.next;
        temp.next = a;
    }

    //works
    public void changeFront(int data){
        long start = System.nanoTime();
        if(head != null){
            head.data = data;
        }
        long finish = System.nanoTime();
        System.out.println("elapsed time " + getDelta(start, finish));
    }

    //works
    public void changeBack(int data){
        long start = System.nanoTime();
        if(tail != null){
            tail.data = data;
        }
        long finish = System.nanoTime();
        System.out.println("elapsed time " + getDelta(start, finish));
    }

    //works
    public void changeNode(int data, int position){
        CirkularListNode temp = head;
        int tempPosition = 0;
        while (tempPosition != position){
            temp = temp.next;
            tempPosition++;
        }
        temp.data = data;
    }

    //works
    public void removeBack(){
        if (head == tail) {
            head = null;
            tail = null;
        } else {
            CirkularListNode current = head;
            while (current.next != tail) {
                current = current.next;
            }
            current.next = null;
            tail = current;
            tail.next = head;
        }
    }

    //works
    public void removeFront(){
        if (head != null){
            head = head.next;
        }
    }

    //works
    public void removeNode(int data) {
        long start = System.nanoTime();
        if(head == null)
            return;

        if (head == tail) {
            head = null;
            tail = null;
            return;
        }

        if (head.data == data) {
            head = head.next;
            tail.next = head;
            return;
        }

        CirkularListNode t = head;       //иначе начинаем искать
        while (t.next != null) {    //пока следующий элемент существует
            if (t.next.data == data) {  //проверяем следующий элемент
                if(tail == t.next) {     //если он последний
                    tail = t;           //то переключаем указатель на последний элемент на текущий
                }
                t.next = t.next.next;   //найденный элемент выкидываем
                return;                 //и выходим
            }
            t = t.next;                //иначе ищем дальше
        }
        long finish = System.nanoTime();
        System.out.println("elapsed time " + getDelta(start, finish));
    }

    //works
    public int sumNodes(){
        long start = System.nanoTime();
        CirkularListNode t = head;
        int sum = 0;
        while (t != null){
            sum += t.data;
            t = t.next;
        }
        long finish = System.nanoTime();
        System.out.println("elapsed time " + getDelta(start, finish));
        return sum;
    }

    //works
    public int getNodeNumber(int data){
        long start = System.nanoTime();
        if (head.data == data){
            return 0;
        }

        int number = 0;
        CirkularListNode t = head;
        while (t.next != null){
            number++;
            if (t.next.data == data){
                return number;
            }
            t = t.next;
        }
        long finish = System.nanoTime();
        System.out.println("elapsed time " + getDelta(start, finish));
        return  -1;
    }

    public void printList() {
        CirkularListNode t = head;
        while (t != null) {
            System.out.print(t.data + " ");
            t = t.next;
        }
    }

    private double getDelta(long start, long finish){
        return (double) (finish - start)/1000000000.0;
    }
}
